import {Client} from 'pg';
require('dotenv').config({path:'C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\app.env'})

export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD, //YOU SHOULD NEVER STORE PASSWORDS IN CODE
    database:process.env.DATABASENAME,
    port:5432,
    host:'34.86.19.9'
});
client.connect()